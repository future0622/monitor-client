/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ericquan8.trojanclient;

/**
 *
 * @author eric
 */
public class Key {

	//服务端ip
    public static String host;
    //服务端端口
    public static int port;
    //本地接受服务端发送命令、文件的端口
    public static int monitor_port;
    
    //默认参数
    public static final int WIDTH = 118;
    public static final int HEIGHT = 95;
    public static final String FILE = "file";
    public static final String COMMAND = "command";
    public static final String COMMAND_NO_REPLY = "command_no_reply";
    public static final String SCREEN = "screen";
    public static final String BROSWER = "broswer";
    public static final String KILL = "kill";
    public static final String OPERATION = "operation";
    public static final int DELAY_TIME = 500;
    public static final int SINGLE_MONITOR_DELAY_TIME = 50;
}
